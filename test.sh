#!/bin/bash
#description   :Test your code against Progtests sample data
#author        :wijnhjan
#date          :20181020
#version       :0.2
#bash_version  :4.3+

function sprintf {
    ((SUPER_SILENT)) || printf "$@"
}

function fullPath {
    echo -n $(! [[ "$1" =~ ^/ ]] && echo $(pwd)/)"$1"
    return 0
}

function show_help {
    ((SUPER_SILENT)) || cat <<HELP

Usage: $0 [-dhsST?] [-c|C args] [-e ext] [-f test_data] [-t timeout] file

Simple script to test sample data from Progtest
against your program.

 file: Path to your executable set for testing
 
 ext: Test file extension. If blank, use ""
 test_data: Folder containing test data
 args: arguments for compiler

 -c Compile source before testing
 -C Compile source before testing with specific compile args
 -d Use expected/got text blocks instead of diff
 -e Set test data extension (default is .txt)
 -f Specify test_data folder location
 -h Display this help text
 -s Silently skip tests with missing output data
 -S Supress all output. Return code is 0 if all tests passed
 -t Time before we kill tested file (default is 1 sec)
 -T Measure run time for every test
 -? Display this help text

HELP
}

OPTIND=1

TEST_EXT=".txt"
TEST_PATH=""
COMPILE_ARGS=""
USE_DIFF=1
SILENT=0
SUPER_SILENT=0
TEST_TIMEOUT=1
COMPILE=0
TIMER=0

while getopts "h?sSdcTC:e:f:t:" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    d)  USE_DIFF=0
        ;;
    c)  COMPILE=1
        ;;
    C)  COMPILE_ARGS=$OPTARG
        COMPILE=1
        ;;
    s)  SILENT=1
        ;;
    S)  SUPER_SILENT=1
        ;;
    e)  TEST_EXT=$OPTARG
        ;;
    f)  TEST_PATH=$OPTARG
        ;;
    t)  TEST_TIMEOUT=$OPTARG
        ;;
    T)  TIMER=1
        ;;
    esac
done

shift $((OPTIND-1))
[ "${1:-}" = "--" ] && shift

if !((SILENT)) && !((SUPER_SILENT)) && command -v curl &>/dev/null && command -v python3 &>/dev/null && command -v sha256sum &>/dev/null; then
    REMOTEURL=https://gitlab.fit.cvut.cz/api/v4/projects/10951/repository/files/test.sh?ref=master
    REMOTEHASH=$(curl -sX GET "$REMOTEURL" | python3 -c "import sys,json; print(json.load(sys.stdin)['content_sha256'])")
    if [[ "$REMOTEHASH" != "$(sha256sum $0 | cut -d' ' -f1)" ]]; then
        printf "\033[0;31mNew version is available!\033[00m\nDownload it from: https://gitlab.fit.cvut.cz/wijnhjan/progtest-test/\n\n"
    fi
fi

[[ -z "$1" ]] && { sprintf "No file specified!\n"; show_help; exit 1; }
[[ -e "$1" ]] || { sprintf "File not found\n"; exit 1; }
RUN_PATH=$(fullPath "$1")
if ((COMPILE)); then
    ((SILENT)) || sprintf "Compiling..\n"
    OUTNAME=$(basename "${RUN_PATH%.*}")
    if ! gcc -Wall -pedantic $COMPILE_ARGS -o "$(fullPath "$OUTNAME")" "$RUN_PATH" -lm ; then
        sprintf "Compilation failed.\n"
        exit 1
    fi
    RUN_PATH="$(fullPath "$OUTNAME")"
elif [[ ! -x "$RUN_PATH" ]]; then
    sprintf "File is not executable.\nTry fixing it with: chmod u+x %s\nor use flag -c to compile source.\n" "$1"
    exit 1
fi

[[ -z "$TEST_PATH" ]] && {
    ((SILENT)) || sprintf "No test_data folder specified, assuming ./test_data/\n\n"
    TEST_PATH="test_data/"
}

TEST_PATH=$(fullPath $TEST_PATH)/
[[ -e "$TEST_PATH" ]] || { echo "$TEST_PATH not found!"; exit 1; }

RUNCOUNT=0
TESTIDS=""

for TEST_NUM in "${TEST_PATH}"*_in$TEST_EXT; do
    [[ -e "$TEST_NUM" ]] || continue
    TID=$(basename "$TEST_NUM" | grep -o '[0-9]\+')
    if [[ -e "$TEST_PATH${TID}_out$TEST_EXT" ]]; then
        ((RUNCOUNT++))
        TESTIDS="$TESTIDS $TID"
    elif [[ $SILENT -eq 0 ]]; then
        sprintf "Test #%d is missing output data, skipping\n" "$(sed 's/^0*//'<<<$TID)"
    fi
done

sprintf "\033[0;33mWill run %d tests\033[0m\n\n" $RUNCOUNT

EXITCODE=0

for TEST_ID in $TESTIDS; do
    if ((TIMER)); then
        sprintf "\n\033[0;34mTime info:\033[0m\n" "$TEST_ID"
        OUT=$(time timeout "$TEST_TIMEOUT" "$RUN_PATH" < "$TEST_PATH${TEST_ID}_in$TEST_EXT")
    else
        OUT=$(timeout "$TEST_TIMEOUT" "$RUN_PATH" < "$TEST_PATH${TEST_ID}_in$TEST_EXT")
    fi
    if cmp -s "$TEST_PATH${TEST_ID}_out$TEST_EXT" <(echo "$OUT"); then
        sprintf "\033[0;32mTest #%s passed\033[0m\n" "$TEST_ID"
    else
        EXITCODE=1
        if ! ((SUPER_SILENT)); then
            printf "\033[0;31mTest #%s failed\033[00m\n" "$TEST_ID"
            if ((USE_DIFF)); then
                printf "\033[0;34mExpected:%$((($(tput cols)/2)-8))s\033[00m\n" "Got:"
                diff -W $(( $(tput cols) - 2 )) -y "$TEST_PATH${TEST_ID}_out$TEST_EXT" <(echo "$OUT")
            else
                printf "\033[0;34mExpected:\033[0m\n"
                cat "$TEST_PATH${TEST_ID}_out$TEST_EXT"
                printf "\n\033[0;34mGot:\033[0m\n%s\n" "$OUT"
            fi
            printf "\n\033[0;33mInput:\033[0m\n"
            cat "$TEST_PATH${TEST_ID}_in$TEST_EXT"
            printf "\n"
        fi
    fi
done
exit $EXITCODE
